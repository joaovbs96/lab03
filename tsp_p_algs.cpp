/*******************************************************************************
 ** VERSION: 1.0
 * MC658 - Projeto e Análise de Algoritmos III - 1s2018
 * Prof: Flavio Keidi Miyazawa
 * PED: Francisco Jhonatas Melo da Silva
 * Usa ideias e código de Mauro Mulati e Flávio Keidi Miyazawa
 ******************************************************************************/

/* IMPLEMENTE AS FUNCOES INDICADAS
 * DIGITE SEU RA: 155951
 * SUBMETA SOMENTE ESTE ARQUIVO */

#include <iostream>
#include <float.h>
#include <lemon/list_graph.h>
#include "mygraphlib.h"
#include "tsp_p_algs.h"
#include <algorithm>
#include "tsp_p_Decoder.h"
#include "MTRand.h"
#include "gurobi_c++.h"
#include "BRKGA.h"
#include <vector>
#include <map>
#include <queue>
#include <random>
#include <algorithm>
#include <lemon/concepts/digraph.h>
#include <lemon/connectivity.h>

clock_t initial_time;

bool naive(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl);

//------------------------------------------------------------------------------
bool constrHeur(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl) {

  DNode v;
  s.tour.clear();
  s.cost = 0.0;
  initial_time = clock();

  s.tour.push_back(l.depot); //caminho começa no depot

  double min_sum_node, sum_ta, current_sum;
  OutArcIt arc_min(l.g, s.tour.back());
  int i;

  sum_ta = 0.0;

  // enquanto não adicionar todos os vértices
  while((int)s.tour.size() < l.n) {

    min_sum_node = DBL_MAX;

    // Se estourou o tempo, a execução é interrompida
    if(clock() - initial_time > tl * CLOCKS_PER_SEC)
      break;

    // Percorre todos os arcos de saída do último vértice adiciona à solução
    for (OutArcIt a(l.g, s.tour.back()); a != INVALID; ++a) {

      i = 0;
      while(i < (int)s.tour.size() && l.g.target(a) != s.tour[i]) i++;
      if(i < (int)s.tour.size()) continue;

      // soma do arco considerando acumulado
      current_sum = l.weight_node[l.g.target(a)] * (sum_ta + l.weight[a]);

      // queremos arco com menor current_sum
      if(current_sum <= min_sum_node) {
        min_sum_node = current_sum;
        arc_min = a;
      }

    }

    // atualizando solução
    // atualiza soma
    sum_ta += l.weight[arc_min];
    s.cost += l.weight_node[l.g.target(arc_min)] * sum_ta;

    // adiciona vértice destino do menor arco
    s.tour.push_back(l.g.target(arc_min));

  }

  // Se não achou sol factível, retorna infinito.
  if((int)s.tour.size() < l.n)
    s.cost = DBL_MAX;

  return false;

}

// função auxiliar de ordenação de lista tabu do alg metaHeur
bool sortSol(pair< Tsp_P_Solution, pair< int, int > > i,
                pair< Tsp_P_Solution, pair< int, int > > j) {
                  return i.first.cost > j.first.cost;
}

//------------------------------------------------------------------------------
bool metaHeur(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl) {

  int i, j;
  initial_time = clock();

  // obtém solução inicial por meio da heuristica brkga
  brkga(l, s, tl);

  Tsp_P_Solution current;
  current.tour = s.tour;
  current.cost = s.cost;

  // só dá pra fazer trocas se tamanho for > 2
  if(l.n <= 2)
    return false;

  // número de candidatas obtidas por iteração
  int numCandidates = 40;

  // queue tabu com swaps mais recentes
  // tamanho do queue é igual número de candidatas
  vector< pair < int, int > > tabu;

  // Condição de parada
  // caso número de iterações sem alteração atinja limite
  int cond = 0;
  int condLimit = l.m * l.n;

  while(true){

    // Se estourou o tempo, a execução é interrompida
    // No pior caso, temos a sol da construtiva como inicial
    if(clock() - initial_time > tl * CLOCKS_PER_SEC)
      return false;

    // encontrar soluções candidatas que fazem randomicamente swap na atual
    vector< pair< Tsp_P_Solution, pair< int, int > > > candidates(numCandidates);

    for(i = 0; i < numCandidates; i++) {

      // fazer cópia de sol atual
      candidates[i].first.tour = current.tour;

      // Obter dois ints aleatórios diferentes de depot e entre si
      // Engine é instanciada apenas uma vez
      random_device rd;
      mt19937 rng(rd());
      uniform_int_distribution<int> uni(0, l.n - 1);

      // Note que essa engine mt19937 funciona como esperado ao ser compilado em
      // sistemas Unix pelo GCC. No Windows, o mesmo só funcionará caso
      // compilado via Visual Studio(aparentemente, a versão de GCC do windows
      // não tem acesso à alguma função de hardware necessária. Nesses casos,
      // serão sempre retornados os mesmos números, de maneira regular.
      // O mesmo poderá cair em um loop, abaixo, sendo encerrado por tempo de
      // execução).

      int swap1 = uni(rng);
      while(l.g.nodeFromId(swap1) == l.depot) {
        swap1 = uni(rng);

        if(clock() - initial_time > tl * CLOCKS_PER_SEC)
          return false;

      }

      int swap2 = uni(rng);
      while((l.g.nodeFromId(swap2) == l.depot) || (swap1 == swap2)) {
        swap2 = uni(rng);

        // Se estourou o tempo, a execução é interrompida
        // No pior caso, temos a sol da construtiva como inicial
        if(clock() - initial_time > tl * CLOCKS_PER_SEC)
          return false;

      }

      // Fazer swap na cópia
      DNode tmpNode = candidates[i].first.tour[swap1];
      candidates[i].first.tour[swap1] = candidates[i].first.tour[swap2];
      candidates[i].first.tour[swap2] = tmpNode;

      // Calcular novo custo
      double sum_ta = 0.0;
      candidates[i].first.cost = 0.0;
      for(j = 1; j < l.n; j++) {
        sum_ta += l.weight[findArc(l.g, candidates[i].first.tour[j - 1], candidates[i].first.tour[j])];
        candidates[i].first.cost += l.weight_node[candidates[i].first.tour[j]] * sum_ta;
      }

    }

    // ordenar candidates
    sort(candidates.begin(), candidates.end(), sortSol);

    // enquanto vetor de sol não for vazio
    while(candidates.size() > 0){
      // pegar solução de peso mínimo dentre as n
      pair< Tsp_P_Solution, pair< int, int > > newCandidate = candidates.back();
      candidates.pop_back();

      // se for melhor que best,
      if(newCandidate.first.cost < s.cost) {
        // atualiza best
        s.tour = newCandidate.first.tour;
        s.cost = newCandidate.first.cost;

        // atualiza atual
        current.tour = newCandidate.first.tour;
        current.cost = newCandidate.first.cost;

        // zera vetor de sols
        candidates.clear();

        // vê se swap está no tabu
        vector< pair <int, int> >::iterator it;
        it = find(tabu.begin(), tabu.end(), newCandidate.second);

        // caso não esteja
        if(it == tabu.end()) {
          // Adiciona swap no tabu
          // se chegou no limite da lista, remover mais antigo.
          if((int)tabu.size() == numCandidates)
            tabu.erase(tabu.begin());

          tabu.push_back(newCandidate.second);
        } else {
          // caso esteja
          // remover e adicionar novamente no fim
          tabu.erase(it);
          tabu.push_back(newCandidate.second);
        }

        cond = 0;

      } else {
        // vê se swap está no tabu
        vector< pair <int, int> >::iterator it;
        it = find(tabu.begin(), tabu.end(), newCandidate.second);

        // caso não esteja
        if (it == tabu.end()){
          // atualiza atual
          current.tour = newCandidate.first.tour;
          current.cost = newCandidate.first.cost;

          // zera vetor de sols
          candidates.clear();

          // adiciona swap no tabu
          // sempre manter apenas numCandidates mais recentes
          // se chegou no limite da lista, remover mais antigo.
          if((int)tabu.size() == numCandidates)
            tabu.erase(tabu.begin());

          tabu.push_back(newCandidate.second);

        } else {
          // caso esteja
          // remover e adicionar novamente no fim
          tabu.erase(it);
          tabu.push_back(newCandidate.second);
        }

        cond++;

        // se atringiu um limite de iterações sem melhora, retorna.
        if(cond == condLimit)
          return false;

      }
    }
  }

  return false;

}

//------------------------------------------------------------------------------
bool brkga(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl) {

  initial_time = clock();

  // Obter floats aleatórios
  // Engine é instanciada apenas uma vez
  const long unsigned rngSeed = time(0); // seed to the random number generator
	MTRand rng(rngSeed); // initialize the random number generator

  tsp_p_Decoder decoder(l);

  // usar const unsgined e const double se usar mais de uma thread
  int chromoCount = l.n; // cromossomos = l.n
  int popSize = 256; // tamanho da pop = 256
  double elite = 0.1; // fração da elite = 0.1 (definido)
  double mutants = 0.1; // fração a ser subsituida = 0.1 (definido)
  double aProb = 0.7; // probabilidade que um descendente herde algo da elite = 0.7 (definido)
  int K = 3; // número de populações independentes = 3 (definido)
  int threads = 1; // número de threads = 1, sem paralelismo(a menos que seja auto, caso sim K = 4)

  // initialize the BRKGA-based heuristic
  BRKGA< tsp_p_Decoder, MTRand > algorithm(chromoCount,
                                              popSize,
                                              elite,
                                              mutants,
                                              aProb,
                                              decoder,
                                              rng,
                                              K,
                                              threads);

  // Variáveis de evolução
  int exchInterval = 100;	// intervalo de troca de elite
  int exchNum = 2;	// quantidade a ser trocada
  int gens = 1000;	// total de gerações

  // Variáveis de restart
  int relevantGen = 1;	// última geração atualizada ou com reset
  int reset = 200;
  vector< double > bestChromo;
  double bestFit = DBL_MAX;

  // loop de evolução
  int currentGen = 1;
  while(currentGen < gens){

    // evolui população em uma geração
    algorithm.evolve();

    // Caso tenha achado geração melhor...
    if(algorithm.getBestFitness() < bestFit) {
      relevantGen = currentGen;
      bestFit = algorithm.getBestFitness();
      bestChromo = algorithm.getBestChromosome();
    }

    // restart
    if((currentGen - relevantGen) > reset) {
      algorithm.reset();	// restart com ramdom keys
      relevantGen = currentGen;
    }

    // troca de elite entre populações
    if(((currentGen % exchInterval) == 0) && (relevantGen != currentGen))
      algorithm.exchangeElite(exchNum);

    currentGen++;

    if(clock() - initial_time > tl * CLOCKS_PER_SEC)
      break;

  }

  // recupera melhor solução
  vector< pair < double, DNode > > bestTour;

  // temos um cromossomo por node
  // obtém vector de pares de (cromossomo, nodes)
  int i = 1;
  bestTour.push_back(make_pair(0, l.depot));
  for (Digraph::NodeIt n(l.g); n != INVALID; ++n)
    if(n != l.depot) {
      pair < double, DNode > newPair (bestChromo[i], n);
      bestTour.push_back(newPair);
      i++;
    }

  // obtém permutação de nodes de acordo com cromossomos
  // manter depot na primeira posição
  sort(bestTour.begin() + 1, bestTour.end());

  // adiciona melhor tour à resposta
  s.tour.clear();
  for(int j = 0; j < l.n; j++)
    s.tour.push_back(bestTour[j].second);

  // Calcular custo
  s.cost = 0.0;
  double sum_ta = 0.0;
  for(int j = 1; j < l.n; j++){
    sum_ta += l.weight[findArc(l.g, s.tour[j - 1], s.tour[j])];
    s.cost += l.weight_node[s.tour[j]] * sum_ta;
  }


  return false;

}

//------------------------------------------------------------------------------
bool exact(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl) {

  // Configura Ambiente
	GRBEnv env = GRBEnv();
	GRBModel model = GRBModel(env);

	// Configra como prob de minimização
	model.set(GRB_IntAttr_ModelSense, GRB_MINIMIZE);

	// Configura limite de tempo
  // Note que o tempo de execução da heurística de cutoff não é contado aqui.
	model.getEnv().set(GRB_DoubleParam_TimeLimit, tl);

	// Configura heuristica/cutoff
	brkga(l, s, tl);
  model.set(GRB_DoubleParam_Cutoff, s.cost);

  Tsp_P_Solution heurSol;
  heurSol.tour = s.tour;
  heurSol.cost = s.cost;

	// zerando solução após execução de heurística
  s.tour.clear();
  s.cost = 0.0;

  Digraph::ArcMap<GRBVar> x(l.g);
  Digraph::NodeMap<GRBVar> v(l.g);

  // Variável Xij
  for(Digraph::ArcIt a(l.g); a != INVALID; ++a)
    x[a] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
  model.update();

  // Variável Vi
  // upper bound infinito por ser acumulado
  for (Digraph::NodeIt n(l.g); n != INVALID; ++n)
    v[n] = model.addVar(0.0, GRB_INFINITY, 0.0, GRB_CONTINUOUS);
  model.update();

  // Adiciona restrições
  // Para cada node Ni, a soma de seus arcos incidentes deve ser = 2
  for (Digraph::NodeIt n(l.g); n != INVALID; ++n) {
    GRBLinExpr exprIn = 0;
    for (Digraph::InArcIt a(l.g,n); a != INVALID; ++a)
      exprIn += x[a];
    model.addConstr(exprIn, GRB_EQUAL, 1);

    GRBLinExpr exprOut = 0;
    for (Digraph::OutArcIt a(l.g,n); a != INVALID; ++a)
      exprOut += x[a];
    model.addConstr(exprOut, GRB_EQUAL, 1);
  }
  model.update();

  // restrições de v
  // Constraint - maxArc -> M da restrição de V
  double maxArc = 0.0;
  for (Digraph::NodeIt n(l.g); n!=INVALID; ++n)
    for (Digraph::InArcIt a(l.g,n); a!=INVALID; ++a)
      if(l.weight[a] > maxArc)
        maxArc = l.weight[a];
  maxArc = maxArc * l.n;

  // Vj >= Vi + Tij - (1 - Xij) * M
  for (Digraph::NodeIt n(l.g); n != INVALID; ++n) {
    if(n != l.depot)
      for (InArcIt a(l.g,n); a != INVALID; ++a){
        GRBLinExpr exprV = v[l.g.source(a)] + l.weight[a] - ((1 - x[a]) * maxArc);
        model.addConstr(v[l.g.target(a)], GRB_GREATER_EQUAL, exprV);
      }
  }
  // V0 = 0
  model.addConstr(v[l.depot], GRB_EQUAL, 0);
  model.update();

  // Configura função objetivo
  GRBLinExpr obj = 0;

  // min sum(p[i]V[i])
  for (Digraph::NodeIt n(l.g); n!=INVALID; ++n)
    obj += l.weight_node[n] * v[n];
  model.update();

  model.setObjective(obj);
  model.optimize();

  // recupera solução
  s.tour.clear();
  s.tour.push_back(l.depot);

  initial_time = clock();
  while((int)s.tour.size() < l.n){
    for (OutArcIt a(l.g, s.tour.back()); a != INVALID; ++a)
      if((1 - x[a].get(GRB_DoubleAttr_X)) < 1)
        if(find(s.tour.begin(), s.tour.end(), l.g.target(a)) == s.tour.end())
          s.tour.push_back(l.g.target(a));

    if(clock() - initial_time > tl * CLOCKS_PER_SEC)
      break;
  }

  // Não encontrou uma solução factível.
  if((int)s.tour.size() < l.n){
    s.cost = DBL_MAX;
    return false;
  }

  // somar custo
  s.cost = 0.0;
  double sum_ta = 0.0;
  for(int j = 1; j < l.n; j++){
    sum_ta += l.weight[findArc(l.g, s.tour[j - 1], s.tour[j])];
    s.cost += l.weight_node[s.tour[j]] * sum_ta;
  }

  // Verificação de retorno
  int statusCode = model.get(GRB_IntAttr_Status);

  // Se estourou o tempo, retorna false
  if(statusCode == 9)
    return false;

  // Não encontrou solução factível, custo infinito e retorna false
  if((statusCode == 3) || (statusCode == 4)){
    s.cost = DBL_MAX;
    return false;
  }

  // Se valor da heurística era menor que o encontrado pelo exato,
  // recupera sol. heurística e retorna false
  if(statusCode == 6){
    s.tour = heurSol.tour;
    s.cost = heurSol.cost;
    return false;
  }

  // Solução é ótima, retornar true
  if(statusCode == 2)
    return true;

  // outras casos: retornar false
  return false;

}


//------------------------------------------------------------------------------
bool naive(const Tsp_P_Instance &instance, Tsp_P_Solution  &sol, int tl)
/*
    *This simple algorithm just computes a tour by choosing the outArc with lowest t_a.
    *This tour, of course, begins at node s (depot);
*/
{
   DNode v, vl;
   sol.tour.clear();
   sol.cost = 0.0;

   v = instance.depot;//tour begins at s
   sol.tour.push_back(v);

   double vl_arc, sum_ta;
   //OutArcIt arc_min;
   OutArcIt arc_min(instance.g, sol.tour.back());
   int /*count =0, */i;


   sum_ta=0.0;

   while((int)sol.tour.size() < instance.n){
        vl_arc = DBL_MAX;

        for (OutArcIt a(instance.g, sol.tour.back()); a != INVALID; ++a){

            i = 0;
            while(i < (int)sol.tour.size() && instance.g.target(a) != sol.tour[i]) i++;
            if(i < (int)sol.tour.size()) continue;

            if(instance.weight[a] <= vl_arc){

                vl_arc = instance.weight[a];
                arc_min = a;
            }
        }
        sum_ta += instance.weight[arc_min];
        sol.cost += instance.weight_node[instance.g.target(arc_min)] * sum_ta;
        vl = instance.g.target(arc_min);
        sol.tour.push_back(vl);

    }
    return false;
}
//------------------------------------------------------------------------------
