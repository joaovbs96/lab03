/*
 * SampleDecoder.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: rtoso
 */

// RA 155951

#include "tsp_p_Decoder.h"
#include "tsp_p.h"

tsp_p_Decoder::tsp_p_Decoder(const Tsp_P_Instance& l) : instance(l) { }

tsp_p_Decoder::~tsp_p_Decoder() { }

double tsp_p_Decoder::decode(const std::vector< double >& chromosome) const {
	vector< pair < double, DNode > > tour;

	// temos um cromossomo por node
	// obtém vector de pares de (cromossomo, nodes)
	int i = 1;
	tour.push_back(make_pair(0, instance.depot));
	for (Digraph::NodeIt n(instance.g); n != INVALID; ++n)
		if(n != instance.depot) {
			pair < double, DNode > newPair (chromosome[i], n);
			tour.push_back(newPair);
			i++;
		}

	// obtém permutação de nodes de acordo com cromossomos
	// manter depot na primeira posição
	sort(tour.begin() + 1, tour.end());

	// Calcular custo
	double sum_ta = 0.0;
	double cost = 0.0;
	for(i = 1; i < instance.n; i++) {
		sum_ta += instance.weight[findArc(instance.g, tour[i - 1].second, tour[i].second)];
		cost += instance.weight_node[tour[i].second] * sum_ta;
	}

	return cost;
}
