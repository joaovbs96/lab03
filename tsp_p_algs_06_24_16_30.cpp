/*******************************************************************************
 ** VERSION: 1.0
 * MC658 - Projeto e Análise de Algoritmos III - 1s2018
 * Prof: Flavio Keidi Miyazawa
 * PED: Francisco Jhonatas Melo da Silva
 * Usa ideias e código de Mauro Mulati e Flávio Keidi Miyazawa
 ******************************************************************************/

/* IMPLEMENTE AS FUNCOES INDICADAS
 * DIGITE SEU RA: 155951
 * SUBMETA SOMENTE ESTE ARQUIVO */

#include <iostream>
#include <float.h>
#include <lemon/list_graph.h>
#include "mygraphlib.h"
#include "tsp_p_algs.h"
#include <algorithm>
#include "tsp_p_Decoder.h"
#include "MTRand.h"
#include "gurobi_c++.h"
#include "BRKGA.h"
#include <vector>
#include <map>
#include <queue>
#include <random>
#include <algorithm>

clock_t initial_time;

// TODO: BRKGA
// TODO: callback do Exato - Talvez não!
// TODO: arrumar bounds do exato

/*
  Tsp_P_Solution
  vector<DNode> tour - vetor de nodes de grafo direcionado
  double        cost - custo total do percurso
  double        lowerBound - bound inferior(default: 0)
  double        upperBound - bound superior(default: inf)

  Tsp_P_Instance
  ListDigraph    g - grafo direcionado(digrafo) D
  int            n - número de nós
  int            m - número de arestas(arcos)
  DNodeStringMap vname - nomes dos vértices
  ArcStringMap   aname - nomes dos arcos
  ArcValueMap    weight - tempo t de cada arco/aresta
  DNodePosMap    posx - posição x do nó
  DNodePosMap    posy - posição y do nó
  DNodeValueMap  weight_node - prioridades p de cada nó

  DNode          depot - nó s de início do ciclo

*/

// 'arcs' são arestas em um digraph(ou directioned graph)

// esse for percorre todas as arestas que saem do último nó adicionado à solução
// for (OutArcIt a(instance.g, sol.tour.back()); a != INVALID; ++a)

// acessando 'destino' da aresta a
// instance.g.target(a)

/* A ideia do Naive é, basicamente, partindo da raíz, percorrer as arestas de
saída e selecionar a de menor peso. Percorrer as de saída do vértice destino,
assim sucessivamente. */

/*

Uma alteração válida no naive é ter o tempo acumulado do caminho percorrido
em mãos e, na hora de selecionar uma aresta, não olhar para seu custo de forma
individual, mas sim o acumulado do tempo * o custo da aresta, como na função
de minimização.

*/

bool naive(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl);

//------------------------------------------------------------------------------
bool constrHeur(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl) {

  DNode v, vl;
  s.tour.clear();
  s.cost = 0.0;
  initial_time = clock();

  v = l.depot; //caminho começa em v
  s.tour.push_back(v);

  double min_sum_node, sum_ta, current_sum;
  OutArcIt arc_min(l.g, s.tour.back());
  int i;

  sum_ta = 0.0;

  // enquanto não adicionar todos os vértices...
  while((int)s.tour.size() < l.n) {

    min_sum_node = DBL_MAX;

    // Percorre todos os arcos de saída do último vértice adiciona à solução
    for (OutArcIt a(l.g, s.tour.back()); a != INVALID; ++a) {

      // Se estourou o tempo, a execução é interrompida
      if(clock() - initial_time > tl * CLOCKS_PER_SEC)
        return false;

      i = 0;
      while(i < (int)s.tour.size() && l.g.target(a) != s.tour[i]) i++;
      if(i < (int)s.tour.size()) continue;

      // soma do arco considerando acumulado
      current_sum = l.weight_node[l.g.target(a)] * (sum_ta + l.weight[a]);

      // queremos arco com menor current_sum
      if(current_sum <= min_sum_node) {

        min_sum_node = current_sum;
        arc_min = a;

      }

    }

    // atualizando solução
    // atualiza soma
    sum_ta += l.weight[arc_min];
    s.cost += l.weight_node[l.g.target(arc_min)] * sum_ta;

    // adiciona vértice destino do menor arco
    vl = l.g.target(arc_min);
    s.tour.push_back(vl);

  }

  return false;

}

bool sortSol(std::pair< Tsp_P_Solution, std::pair< int, int > > i,
                std::pair< Tsp_P_Solution, std::pair< int, int > > j) { return i.first.cost > j.first.cost; }
//------------------------------------------------------------------------------
bool metaHeur(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl) {

  int i, j;
  initial_time = clock();

  // obtém solução inicial por meio da heuristica construtiva
  constrHeur(l, s, tl);
  std::cout << "initcost: " << s.cost << std::endl;

  Tsp_P_Solution current;
  current.tour = s.tour;
  current.cost = s.cost;

  // queue com n swaps mais recentes
  std::vector<std::pair < int, int > > tabu;

  // cond: se best não for atualizada por 50% do tamanho do tempo limite, false
  clock_t cond_time = clock();

  while(true){
    if(clock() - initial_time > tl * CLOCKS_PER_SEC)
      return false;

    // encontrar n soluções candidatas que fazem randomicamente 1 swap na atual
    std::vector< std::pair< Tsp_P_Solution, std::pair< int, int > > > candidates(10);
    // por n vezes,
    for(i = 0; i < 10; i++){
      // fazer cópia de sol
      candidates[i].first.tour = current.tour;

      // pegar dois ints aleatórios diferentes de depot e entre si
      std::random_device rd; // only used once to initialise (seed) engine
      std::mt19937 rng(rd()); // random-number engine used (Mersenne-Twister in this case)
      std::uniform_int_distribution<int> uni(0,l.n - 1); // guaranteed unbiased
      // esse método de rand não funciona no windows TODO: achar um outro?

      int swap1 = uni(rng);
      while(l.g.nodeFromId(swap1) == l.depot) {
        swap1 = uni(rng);
      }

      int swap2 = uni(rng);
      while((l.g.nodeFromId(swap2) == l.depot) || (swap1 == swap2)) {
        swap2 = uni(rng);
      }

      std::cout << "PostRand" << std::endl;

      // fazer swap na cópia
      DNode tmpNode = candidates[i].first.tour[swap1];
      candidates[i].first.tour[swap1] = candidates[i].first.tour[swap2];
      candidates[i].first.tour[swap2] = tmpNode;

      std::cout << "PostSwap" << std::endl;

      // calcular novo custo
      // obtém soma
      double sum_ta = 0.0;
      candidates[i].first.cost = 0.0;
      for(j = 1; j < l.n; j++){
        sum_ta += l.weight[findArc(l.g, candidates[i].first.tour[j], candidates[i].first.tour[j])];
        candidates[i].first.cost += l.weight_node[candidates[i].first.tour[j]] * sum_ta;
      }

      std::cout << "PostSum" << std::endl;

    }

    // ordenar candidates
    sort(candidates.begin(), candidates.end(), sortSol);

    // enquanto vetor de sol não for vazio
    while(candidates.size() > 0){
      // pegar solução de peso mínimo dentre as n
      std::pair< Tsp_P_Solution, std::pair< int, int > > newCandidate = candidates.back();
      candidates.pop_back();

      // se for melhor que best,
      if(newCandidate.first.cost < s.cost){
        // atualiza best
        s.tour = newCandidate.first.tour;
        s.cost = newCandidate.first.cost;

        // atualiza atual
        current.tour = newCandidate.first.tour;
        current.cost = newCandidate.first.cost;

        // zera vetor de sols
        candidates.clear();

        // vê se swap está no tabu
        std::vector<std::pair <int, int> >::iterator it;
        it = find(tabu.begin(), tabu.end(), newCandidate.second);

        // caso não esteja
        if (it == tabu.end()){

          // adiciona swap no tabu
          if((int)tabu.size() == 10){
            std::cout << "PreRotate" << std::endl;
            std::rotate(tabu.begin(), tabu.begin()+1, tabu.end());
            std::cout << "PosRotate" << std::endl;
            tabu.pop_back();
            std::cout << "PosPop" << std::endl;
          }

          tabu.push_back(newCandidate.second);
          std::cout << "PosPush" << std::endl;
        }

        cond_time = clock(); // reseta contador cond

      } else {
        // vê se swap está no tabu
        std::vector<std::pair <int, int> >::iterator it;
        it = find(tabu.begin(), tabu.end(), newCandidate.second);

        // caso não esteja
        if (it == tabu.end()){
          // atualiza atual
          current.tour = newCandidate.first.tour;
          current.cost = newCandidate.first.cost;

          // zera vetor de sols
          candidates.clear();

          // adiciona swap no tabu
          if((int)tabu.size() == 10){
            std::cout << "PreRotate" << std::endl;
            std::rotate(tabu.begin(), tabu.begin()+1, tabu.end());
            std::cout << "PosRotate" << std::endl;
            tabu.pop_back();
            std::cout << "PosPop" << std::endl;
          }

          tabu.push_back(newCandidate.second);
          std::cout << "PosPush" << std::endl;
        }
      }
    }

   }

   return false;

}

//------------------------------------------------------------------------------
bool brkga(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl) {
   return naive(l, s, tl);
}

//------------------------------------------------------------------------------
bool exact(const Tsp_P_Instance &l, Tsp_P_Solution  &s, int tl) {

  int i, j;
  initial_time = clock();

  // Configura Ambiente
	GRBEnv env = GRBEnv();
	GRBModel model = GRBModel(env);

	// Configra como prob de minimização
	model.set(GRB_IntAttr_ModelSense, GRB_MINIMIZE);

	// Configura limite de tempo
	model.getEnv().set(GRB_DoubleParam_TimeLimit, tl);

	// Desligar Presolve: melhora performance ao utilizar cutoff
	model.set("Presolve", "0");

	// Configura heuristica/cutoff
	constrHeur(l, s, tl);
  model.set(GRB_DoubleParam_Cutoff, s.cost);

	// zerando solução após execução de heurística
  s.tour.clear();
  s.cost = 0.0;

  // mapeia vértices
  vector<DNode> nodesVector;
  nodesVector.push_back(l.depot); // sempre o primeiro
  for(Digraph::NodeIt n(l.g); n!= INVALID; ++n)
    if(n != l.depot)
      nodesVector.push_back(n);

  map<DNode, int> mapToVector;
  for(i = 0; i < l.n; i++)
    mapToVector[nodesVector[i]] = i;

  vector< vector<OutArcIt> > edgesMatrix(l.n, vector<OutArcIt>(l.n));
  for(i = 0; i < l.n; i++) {
    j = 0;
    for (OutArcIt a(l.g, nodesVector[i]); a != INVALID; ++a) {
      edgesMatrix[i][j] = a; // a é a aresta [ij]
      j++;
    }
  }

  // Adiciona variáveis x e v
	vector<GRBVar> v(l.n); // v = acumulado de peso
	vector< vector<GRBVar> > x(l.n, vector<GRBVar>(l.n)); // x = 1 se aresta na sol

  for(i = 0; i < l.n; i++){
    v[i] = model.addVar(0.0, 1.0, l.weight_node[nodesVector[i]], GRB_CONTINUOUS);

    for(j = 0; j <= i; j++){
      x[i][j] = model.addVar(0.0, 1.0, l.weight[edgesMatrix[i][j]], GRB_BINARY);
      x[j][i] = x[i][j];
    }
	}
	model.update();

  cout << "matriz n ok" << endl;

  // Adiciona restrições
  // Constraint - MaxArc
  double maxArc = 0.0;
  for(i = 0; i < l.n; i++)
    for(j = 0; j < l.n; j++)
      if(l.weight[edgesMatrix[i][j]] > maxArc)
        maxArc = l.weight[edgesMatrix[i][j]];
  maxArc = maxArc * l.n * 2;

  cout << "matriz ok" << endl;

  // restrição de x
  for(i = 0; i < l.n; i++){
    GRBLinExpr exprSumX = 0;
    for(j = 0; j < l.n; j++)
        exprSumX += x[i][j];
    model.addConstr(exprSumX == 1); // sum(Xi) = 1 ptq i

  }
  model.update();

  // Vj >= Vi + Tij - (1 - Xij) * maxArc
  for(i = 0; i < l.n; i++) {
    for(j = 0; j < l.n; j++) {
      GRBLinExpr exprV = v[i] + l.weight[edgesMatrix[i][j]] - ((1 - x[i][j]) * maxArc);
      model.addConstr(v[j], GRB_GREATER_EQUAL, exprV);
    }
  }

  // Configura função objetivo
  GRBLinExpr obj = 0;

  // sum(p[i]V[i])
  for(i = 0; i < l.n; i++)
    obj += l.weight_node[nodesVector[i]] * v[i];

  model.setObjective(obj);
  model.optimize();

  std::cout << "Teste1" << std::endl;
  // recupera solução
  while((int)s.tour.size() < l.n){
    s.tour.push_back(l.depot);

    for (OutArcIt a(l.g, s.tour.back()); a != INVALID; ++a){
      // mapToVector[nodes] retorna indice de node
      i = mapToVector[l.g.source(a)];
      j = mapToVector[l.g.target(a)];

      std::cout << i << "," << j << std::endl;

      if(x[i][j].get(GRB_DoubleAttr_X) > 0){
        s.tour.push_back(l.g.target(a));
        break;
      }

    }
  }

  std::cout << "Teste2" << std::endl;

  // rotaciona até depot ficar no começo. TODO: confirmar
  /*while(s.tour[0] != l.depot){
    std::rotate(s.tour.begin(), s.tour.begin()+1, s.tour.end());
  }*/

  std::cout << "Teste3" << std::endl;
  // obtém soma
  double sum_ta = 0.0;
  for(i = 1; i < l.n; i++){
    sum_ta += l.weight[findArc(l.g, s.tour[i], s.tour[i - 1])];
    s.cost += l.weight_node[s.tour[i]] * sum_ta;
  }

  std::cout << "Teste3" << std::endl;

  // Se estourou o tempo, retorna false
  if(clock() - initial_time > tl * CLOCKS_PER_SEC)
    return false;
  return true;
}


//------------------------------------------------------------------------------
bool naive(const Tsp_P_Instance &instance, Tsp_P_Solution  &sol, int tl)
/*
    *This simple algorithm just computes a tour by choosing the outArc with lowest t_a.
    *This tour, of course, begins at node s (depot);
*/
{
   DNode v, vl;
   sol.tour.clear();
   sol.cost = 0.0;

   v = instance.depot;//tour begins at s
   sol.tour.push_back(v);

   double vl_arc, sum_ta;
   // OutArcIt arc_min;
   OutArcIt arc_min(instance.g, sol.tour.back());
   int /*count =0, */i;


   sum_ta=0.0;

   while((int)sol.tour.size() < instance.n){
        vl_arc = DBL_MAX;

        for (OutArcIt a(instance.g, sol.tour.back()); a != INVALID; ++a){

            i = 0;
            while(i < (int)sol.tour.size() && instance.g.target(a) != sol.tour[i]) i++;
            if(i < (int)sol.tour.size()) continue;

            if(instance.weight[a] <= vl_arc){

                vl_arc = instance.weight[a];
                arc_min = a;
            }
        }
        sum_ta += instance.weight[arc_min];
        sol.cost += instance.weight_node[instance.g.target(arc_min)] * sum_ta;
        vl = instance.g.target(arc_min);
        sol.tour.push_back(vl);

    }
    return false;
}
//------------------------------------------------------------------------------
